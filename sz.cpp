using namespace std;

#include <defs.p>
#include <stdio.h>
#include <regex.h>
#include <string>
#include <libgen.h>
#include <limits.h>
#include <assert.h>

extern "C"
{
   #include <linux.h>
   #include <linuxsp.h>
   #include <logger.h>
   #include <zmodem.h>
}

#include <GetOpt.oop>
#include <StdioPort.oop>

/* program descriptor to greet user */
#define LEADER  "ZModem Send Utility [SwiftWare]"
#define VERSION "$Revision: 1.2 $  $Date: 2019/03/11 19:33:07 $"

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright (C) Dana Swift
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
/** RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

// function prototypes
void usage(const char *prgname, const char *cmdargs);
int  configure(char *prgname, string &logpath, string &com, unsigned long &baudrate,
               bool &autolog, time_t &TimeOut);
int  ZModemSend(const char *prgname, ZModem *zmodem);

// initialize crash-recovery feature
bool CrashRecovery=false;
bool RemoteCrashRecoveryEnable=true;
bool RtsCtsEnable=false;
bool CdEnable=false;
int  delay=0;

/*========================================================================*/
/* program to receive a file using the variants of the xmodem protocol    */
/*========================================================================*/
/**
  This program is the sender for the zmodem protocol.  Configuration
  of the utility can be done with command line arguments or with a
  configuration file in the user's home directory.  The configuration
  file is located at $(HOME)/.$(basename(argv[0]))rc where $(HOME) is
  read from the environment variables and where $(basename(argv[0]))
  is the basename (ie., pathname less the directory portion) of this
  program.  For example, if user foo's home directory is '/home/foo'
  and foo runs this program as '/usr/local/bin/sz' then the pathname
  for the configuration file is '/home/foo/.szrc'.  An example of this
  configuration file is:

      \begin{verbatim}
      # specify the serial port device
      ComDev=/dev/com1
      
      # specify the baud rate (ignored if ZModem transfer is via stdio)
      BaudRate=115200
      
      # enable (RtsCtsEnable!=0) or disable (RtsCtsEnable==0) RTS/CTS hardware handshaking
      RtsCtsEnable=0

      # enable or disable carrier detection 
      CarrierDetectEnable=0

      # specify the name of the log file
      LogPath=/app/swift/logs/szlog
      
      # enable (AutoLog!=0) or disable (AutoLog==0) the auto-log feature
      AutoLog=1
      
      # request crash-recovery by the receiver
      CrashRecovery=0

      # set the ZModem timeout period (seconds)
      TimeOut=80

      # set the delay period (seconds) prior to ZModem session
      ZModemDelay=0

      # set the default debug level (range: 0-4)
      DebugBits=0x0002
     \end{verbatim}

  For configuration by command line arguments, invoke this program with the
  '-h' option.

  written by Dana Swift
*/
int main(int argc, char **argv, char **envp)
{
   // used to read file to be transferred
   FILE *source=NULL;

   // initialize the baud rate of the SerialPort
   unsigned long baudrate=9600;
   
   // initialize auto-log feature
   bool autolog=false;
 
   // pathname to the log file
   string logpath;

   // pathname to the serial port device
   string comdev;

   // ZModem timeout period
   time_t TimeOut=60;
   
   // define the command line options string
   const char *cmdargs="b:c:d:ehl:p:qrt:vw:xy";

   // define the command line option processor
   GetOpt getopt(argc,argv,cmdargs);

   // check for usage query
   if (argc<2) usage(argv[0],cmdargs);

   // initialize logging facility
   LinuxLogInit();

   // read the user's configuration file
   configure(argv[0],logpath,comdev,baudrate,autolog,TimeOut);

   // loop to process command line arguments
   for (int option; (option = getopt()) != EOF;) 
   {
      switch (option)
      {
         // set the baud rate
         case 'b': {baudrate=atoi(getopt.optarg); break;}
            
         // set the serial port device
         case 'c': {comdev=getopt.optarg; break;}
         
         // set the debug level
         case 'd': {debugbits=strtoul(getopt.optarg,NULL,16); break;}

         // enable enhanced debugging
         case 'e': {debugbits|=ZMODEM_H; break;}
            
         // print the usage query
         case 'h': {usage(argv[0],cmdargs); break;}

         // configure a static log file
         case 'l': {logpath=getopt.optarg; autolog=false; break;}

         // configure an auto-log file
         case 'p': {logpath=getopt.optarg; autolog=true; break;}

         // set quiet mode for log entries
         case 'q': {debugbits=0; break;}

         // enable crash recovery
         case 'r': {CrashRecovery=true; break;}

         // set the ZModem timeout
         case 't': {TimeOut=atoi(getopt.optarg); break;}

         // increase the verbosity of log entries
         case 'v': {if (!(debugbits|ZMODEM_H)) {debugbits|=ZMODEM_H;}
                    else if (debuglevel<5) {debugbits++;} break;}

         // set an optional delay (seconds) before ZModem session
         case 'w':
         {
            // get the delay period (seconds)
            delay=atoi(getopt.optarg);

            // condition the delay period
            if (delay<=0) {delay=0;} else if (delay>10) {delay=10;}

            break;
         }
         
         // enable hardware handshaking
         case 'x': {RtsCtsEnable=true; break;}
            
         // enable carrier detection
         case 'y': {CdEnable=true; break;}

         // set a trap for unrecognized options
         default: {LogEntry(argv[0],"Unrecognized option: -%c\n",option);}
      }
   }

   // check if a debug log was requested
   if (logpath.length())
   {
      // check of autolog feature is enabled
      if (autolog) LogOpenAuto(logpath.c_str());

      // open a statically-named log
      else LogOpen(logpath.c_str(),'w'); 
   }

   // check criteria for writing command line to engineering log
   if (debuglevel>=1)
   {
      // print LEADER to log file 
      LogEntry(argv[0],"%s\n",LEADER);

      // print VERSION to the log stream
      LogEntry(argv[0],"%s\n",VERSION);

      // print the command line to the log stream
      LogEntry(argv[0],"Cmd Line:");
 
      // write command line to log stream
      for (int len=11,i=0; i<argc; i++)
      {
         // compute the length of the current command line
         len += strlen(argv[i])+1;

         // check if it's time to wrap to the next line
         if (len>75)
         {
            // wrap the command line around to the next line
            LogAdd("\n"); len=10+strlen(argv[0]);
            LogEntry(argv[0],"%*s",len," ");
         }

         // write the i(th) command line argument to the log stream
         LogAdd(" %s",argv[i]); 
      }

      // terminate the command line in the log stream
      LogAdd("\n");
   }

   // check if the output file name was included
   if (!argv[getopt.optind]) 
   {
      // make a log entry that the output file was missing
      LogEntry(argv[0],"Missing input file name.\n");
   }

   // check if the output file can be opened
   else if (!(source=fopen(argv[getopt.optind],"r")))
   {
      // make a log entry that the output file was missing
      LogEntry(argv[0],"Unable to open input file [%s].\n",
               argv[getopt.optind]);
   }

   // proceed with the ZModem transfer 
   else
   {
      /* initialize the ZModem structure with transfer parameters */
      ZModem *zmodem = ZModemTimeOut(ZModemInit(NULL,source,argv[getopt.optind]),TimeOut);

      /* check criteria for crash-recovery */
      if (CrashRecovery) ZModemCrashRecovery(zmodem);

      /* check if a serial port was specified */
      if (comdev.length())
      {
         /* open the SerialPort */
         if ((zmodem->com=sopen(comdev.c_str(),baudrate,0)))
         {
            /* assert DTR */
            if (zmodem->com->dtr) {zmodem->com->dtr(1);}

            /* check for carrier detection */
            if (!CdEnable)     {((SerialPort *)zmodem->com)->cd=NULL;}

            /* check for RTS/CTS hardware handshaking */
            if (!RtsCtsEnable)
            {
               ((SerialPort *)zmodem->com)->rts=NULL;
               ((SerialPort *)zmodem->com)->cts=NULL;
            }
            
            /* assert the RTS signal */
            else if (zmodem->com->rts) {zmodem->com->rts(1);}
            
            /* receive the file */
            ZModemSend(argv[0],zmodem);
         }
         else {LogEntry(argv[0],"SerialPort failed to open.\n");}
      }
   
      // create a StdioPort to emulate a SerialPort and begin transfer
      else {StdioPort stdio; zmodem->com=(&stdio.port); ZModemSend(argv[0],zmodem);}
   } 
   
   // close the log file
   if (logpath.length()) LogClose();
   
   return 0;
}

/*------------------------------------------------------------------------*/
/* transmit the file using the ZModem protocol                            */
/*------------------------------------------------------------------------*/
int ZModemSend(const char *prgname, ZModem *zmodem)
{
   /* define the logging signature */
   cc *FuncName = "ZModemSend()";

   // initialize the return value
   int status=-1;

   // validate the logging signature of the ZModem send utility
   assert(prgname);

   // validate the ZModem structure
   if (zmodem)
   {
      // check for delay prior to ZModem session 
      if (delay>0 && delay<=10) {Wait(delay*1000);}
      
      // transmit the file using the ZModem protocol
      if ((status=zTx(zmodem))>0)
      {
         // log metadata for the successful ZModem transfer
         LogEntry(prgname,"ZModem Success: \"%s\" [bytes:%ld, sec:%0.1f, bps:%0.0f]\n",
                  zmodem->fname,zmodem->nbytes,zmodem->etime,
                  (zmodem->etime>0)?(zmodem->nbytes/zmodem->etime):0);
      }
      else {LogEntry(prgname,"ZModem failure: %s\n",(zmodem->fname[0])?zmodem->fname:"");}
   }
   else {LogEntry(FuncName,"Invalid ZModem structure.\n");}

   return status;   
}

/*------------------------------------------------------------------------*/
/* function to read a configuration file                                  */
/*------------------------------------------------------------------------*/
int configure(char *prgname, string &logpath, string &com, unsigned long &baudrate,
              bool &autolog, time_t &TimeOut)
{
   int status = -1;
   const char *home=NULL;
   char buf[1024];
   FILE *source;
   
   // validate the function parameter that points to the environment
   if (!prgname) LogEntry("configure()","NULL pointer to program name.\n");

   // get the home directory from the environment
   else if (!(home=getenv("HOME")))
   {
      LogEntry("configure()","Environment doesn't contain HOME.\n"); status=0;
   }
   
   else
   {
      // reinitialize the return value of this function
      status=0;
      
      // create the name of the configuration file
      sprintf(buf,"%s/.%src",home,basename(prgname));

      // open the configuration file
      if ((source=fopen(buf,"r")))
      {
         /* define the number of subexpressions in the regex pattern */
         #define NSUB 2

         /* define objects needed for regex matching */
         regex_t regex; regmatch_t regs[NSUB+1];
      
         #define WS  "[ \t]*"
         #define VAR "([a-zA-Z0-9]+)"
         #define VAL "([^ \t\r\n#]+)"

         /* construct the regex pattern string for files with message locks */ 
         const char *pattern = "^" WS VAR WS "=" WS VAL;
               
         /* compile the regex pattern */
         assert(!regcomp(&regex,pattern,REG_EXTENDED|REG_NEWLINE));

         /* protect against segfaults */
         assert(NSUB==regex.re_nsub);

         while (fgets(buf,1023,source))
         {
            // check if the current line matches the regex
            if (regexec(&regex,buf,regex.re_nsub+1,regs,0)) continue;

            // extract the variable name
            string var; var = extract(buf,regs[1].rm_so+1,regs[1].rm_eo-regs[1].rm_so);

            // extract the value
            string val; val = extract(buf,regs[2].rm_so+1,regs[2].rm_eo-regs[2].rm_so);

            // make a log entry
            if (debuglevel>=3) LogEntry("configure()","%s = %s ",var.c_str(),val.c_str());

            // check for autolog configurator
            if (!strcmp("AutoLog",var.c_str()))
            {
               // initialize the value of the autolog switch
               autolog = (atoi(val.c_str())) ? true : false;

               // make a log entry
               if (debuglevel>=3) LogAdd("[Autolog %s]\n",(autolog)?"enabled":"disabled");
            }

            // check for autolog configurator
            else if (!strcmp("BaudRate",var.c_str()))
            {
               // initialize the baud rate
               baudrate = atol(val.c_str());

               // make a log entry
               if (debuglevel>=3) LogAdd("[BaudRate %ld]\n",baudrate);
            }

            // check for autolog configurator
            else if (!strcmp("ComDev",var.c_str()))
            {
               // set the serial port device
               com=val; if (debuglevel>=3) LogAdd("[ComDev(%s)].\n",com.c_str());

               // check for default output
               if (com=="stdio") {com.clear();}
            }

            // check for logpath configurator
            else if (!strcmp("LogPath",var.c_str()))
            {
               // set the logpath
               logpath=val; if (debuglevel>=3) LogAdd("[logpath(%s)].\n",logpath.c_str());
            }

            // check for logpath configurator
            else if (!strcmp("CrashRecovery",var.c_str()))
            {
               // initialize the value of the crash-recovery switch
               CrashRecovery = (atoi(val.c_str())) ? true : false;

               // make a log entry
               if (debuglevel>=3) LogAdd("[CrashRecovery %s]\n",
                                         (CrashRecovery)?"enabled":"disabled");
            }

            // check for logpath configurator
            else if (!strcmp("RemoteCrashRecoveryEnable",var.c_str()))
            {
               // initialize the value of the crash-recovery switch
               RemoteCrashRecoveryEnable = (atoi(val.c_str())) ? true : false;

               // make a log entry
               if (debuglevel>=3) LogAdd("[RemoteCrashRecoveryEnable %s]\n",
                                         (RemoteCrashRecoveryEnable)?"enabled":"disabled");
            }

            // check for RTS/CTS hardware handshaking
            else if (!strcmp("RtsCtsEnable",var.c_str()))
            {
               // initialize the value of the RTS/CTS switch
               RtsCtsEnable = (atoi(val.c_str())) ? true : false;

               // make a log entry
               if (debuglevel>=3) LogAdd("[RtsCtsEnable %s]\n",
                                         (RtsCtsEnable)?"enabled":"disabled");
            }

            // check for carrier detection
            else if (!strcmp("CarrierDetectEnable",var.c_str()))
            {
               // initialize the value of the carrier detect switch
               CdEnable = (atoi(val.c_str())) ? true : false;

               // make a log entry
               if (debuglevel>=3) LogAdd("[CarrierDetectEnable %s]\n",
                                         (CdEnable)?"enabled":"disabled");
            }

            // check for debuglevel configurator
            else if (!strcmp("DebugBits",var.c_str()))
            {
               // get the debuglevel
               debugbits = (strtoul(val.c_str(),NULL,16));

               // make a log entry
               if (debuglevel>=3) LogAdd("[debugbits(0x%04x) debuglevel(%d)].\n",
                                         debugbits,debuglevel);
            }

            // check for autolog configurator
            else if (!strcmp("TimeOut",var.c_str()))
            {
               // initialize the ZModem timeout period
               TimeOut = atoi(val.c_str());

               // make a log entry
               if (debuglevel>=3) LogAdd("[TimeOut %ld]\n",TimeOut);
            }

            // check for autolog configurator
            else if (!strcmp("ZModemDelay",var.c_str()))
            {
               // initialize the ZModem timeout period
               delay = atoi(val.c_str());

               // condition the delay period
               if (delay<=0) {delay=0;} else if (delay>10) {delay=10;}
               
               // make a log entry
               if (debuglevel>=3) LogAdd("[ZModemDelay %ld]\n",delay);
            }

            // warn of an unimplemented configurator
            else if (debuglevel>=3) LogAdd("[not implemented].\n");
         }

         // reinitialize function's return value
         status=1;
         
         /* clean up the regex pattern buffer and registers */
         regfree(&regex); 
      }

      // warn the user that the config file couldn't be opened
      else LogEntry("configure()","Attempt to open configuration file (%s) failed.\n",buf);
   }
   
   return status;
}
   
/*------------------------------------------------------------------------*/
/* function to print the usage-query for this program                    */
/*------------------------------------------------------------------------*/
void usage(const char *prgname, const char *cmdargs)
{
   fprintf(stderr,"# " LEADER "\n# " VERSION "\n"
           "usage: %s [%s] file\n"
           "  -b  Select the SerialPort's baud rate. [9600]\n"
           "  -c  Set the SerialPort. [stdio]\n"
           "  -d  Set the logging verbosity (range: 0-5). [2]\n"
           "  -e  Enable enhanced logging.\n"
           "  -h  Print this usage query.\n"
           "  -l  Pathname for log file.\n"
           "  -p  Base pathname used for automatically generated name of log file.\n"
           "  -q  Quiet mode - suppresses verbosity of log entries.\n"
           "  -r  Enable crash recovery.\n"
           "  -t  Set the ZModem timeout period (seconds). [60]\n"
           "  -v  Increase verbosity of log entries.  More v's generate more output.\n"
           "  -w  Delay ZModem session (seconds). [0]\n"
           "  -x  Enable RTS/CTS hardware handshaking.\n"
           "  -y  Enable carrier detection.\n"
           "\n",prgname,cmdargs);

   exit(0);
}
