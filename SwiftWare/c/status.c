/****************************************/
/*     member of utils library          */
/****************************************/
#include <defs.p>

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * RCS Log:
 *
 * $Log$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/*========================================================================*/
/*   procedure to set individual status bits                              */
/*========================================================================*/
void set_bit(int *status,int bit)
{
   int one=1;

   /* set bit high */
   if (bit>0 && bit<=16) {*status = *status | (one<<(bit-1));}
   
   /* set all bits low and reinitialize status_ array */
   else if (bit==0) {*status = 0;}
   
   /* set bit low */
   else if (bit>=-16 && bit<0) {*status = *status & (~(one << ( (bit*=-1) - 1)) );}

   else {swifterr("set_bit(%d): bit operation out of range [-16 to 16]\n",bit);}
}

/*========================================================================*/
/*   function to determine if integer bit is high of low                  */
/*========================================================================*/
int get_bit(int status,int bit)
{
   int one=1;

   if (bit<=0 || bit>16) {swifterr("bit_set(%d): bit operation out of range [1 to 16]\n",bit);}

   if ( status & (one<<(bit-1)) ) return(1);
   else return(0);
}
