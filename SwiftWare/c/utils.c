/****************************************/
/*     member of utils library          */
/****************************************/
#include <defs.p>

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * RCS Log:
 *
 * $Log$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

/* define scratch buffer */
char scrbuf[MAXSTRLEN+1]="";

/*========================================================================*/
/* function to check for null pointer and return message                  */
/*========================================================================*/
void validate(void *ptr,const char *ptr_name,const char *func_name)
{
   if (!ptr) swifterr("ERROR [%s]: Unable to validate pointer \"%s\".\n",func_name,
ptr_name);
}

/*========================================================================*/
/*  procedure to print program LEADER string to output stream             */
/*========================================================================*/
void doLeader(char *leader,FILE *dest)
{
   /* make sure we get a valid file pointer */
   FILE *dst = (dest) ? dest : swiftware_stderr;

   /* print leader to dest stream */
   if (leader) fprintf(dst,"%s%s\n",((dst==stdout)?"$ ":"\n"),leader);
}

/*========================================================================*/
/* procedure to write formatted error message to stderr and exit          */
/*========================================================================*/
/* 
  This function behaves identically to the "printf" function except for one
  difference.  It writes the message to the "swifterr_stderr" stream rather 
  than the "stdout" stream.

  For information on what the format codes are, see the standard c-library
  reference manual for "printf".

  written by Dana Swift 3/20/95

*/
void message(const char *format,...)
{
   va_list arg_pointer;

   /* get the format argument from the argument list */
   va_start(arg_pointer,format);

   /* print the message */
   vfprintf(swiftware_stderr,format,arg_pointer);

   /* clean up call */
   va_end(arg_pointer);

   /* flush the output */
   fflush(swiftware_stderr);
}

/*========================================================================*/
/* procedure to write formatted error message to stderr and exit          */
/*========================================================================*/
/* 
  This function behaves identically to the "printf" function except for two
  differences.  1) It exits the program after printing its message.  2) It 
  writes the message to the "stderr" stream rather than the "stdout" stream.

  For information on what the format codes are, see the standard c-library
  reference manual for "printf".

  written by Dana Swift 6/21/91

*/
void swifterr(const char *format,...)
{
   va_list arg_pointer;

   /* get the format argument from the argument list */
   va_start(arg_pointer,format);

   /* print the message */
   vfprintf(swiftware_stderr,format,arg_pointer);

   /* clean up call */
   va_end(arg_pointer);

   /* flush the output */
   fflush(swiftware_stderr);

   exit(0);
}

/*========================================================================*/
/*  function to convert string to lower case                              */
/*========================================================================*/
char *strlwr(char rec[])
{
   int i;

   for (i=0; rec && rec[i]; i++)
   {
      rec[i]=lower_case(rec[i]);
   }

   return(rec);
}

/*========================================================================*/
/*  function to convert string to upper case                              */
/*========================================================================*/
char *strupr(char rec[])
{
   int i;

   for (i=0; rec && rec[i]; i++)
   {
      rec[i]=upper_case(rec[i]);
   }

   return(rec);
}
