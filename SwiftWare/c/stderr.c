/****************************************/
/*     member of utils library          */
/****************************************/
#include <defs.p>

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * RCS Log:
 *
 * $Log$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

FILE *swiftware_stderr_ptr = NULL;

/*========================================================================*/
/* function to return a pointer to the swiftware stderr stream            */
/*========================================================================*/
FILE *get_stderr(void)
{
   return (swiftware_stderr_ptr) ? swiftware_stderr_ptr : stderr;
}

/*========================================================================*/
/* procedure to set destination of messages intended for stderr           */
/*========================================================================*/
void cmd_stderr(int *argc,char *argv[])
{
   int i,j;
   char *tmp;

   for (i=1; i<*argc; i++)
   {
      /* copy command line argument to scratch buffer for further processing */
      copy(argv[i],1,MAXSTRLEN,scrbuf);

      /* convert to lower case */
      strlwr(scrbuf);

      /* does command line argument change stderr to stdout? */
      if (!strncmp("stderr=",scrbuf,7) && !strncmp("stdout",&argv[i][7],6))
      {
         swiftware_stderr_ptr=stdout;
         break;
      }

      #ifdef IBM

         /* does command line argument change stderr to stdprn? */
         if (!strncmp("stderr=",scrbuf,7) && !strncmp("stdprn",&argv[i][7],6))
         {
            swiftware_stderr_ptr=stdprn;
            break;
         }

      #endif

      /* does command line argument change stderr to a disk file? */
      if (!strncmp("stderr=",scrbuf,7))
      {
         FILE *fp = NULL;

         /* attempt to open file */
         if ((fp=fopen(&argv[i][7],"w"))) {swiftware_stderr_ptr=fp; break;}
      }
   }

   if (i==(*argc)) return;

   /* rearrange command line arguments so "stderr=..." is at the end */
   tmp=argv[i]; for(j=i; j<(*argc)-1; j++) argv[j] = argv[j+1]; argv[j]=tmp; (*argc)--;
}
