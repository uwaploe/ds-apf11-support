#ifndef PROFILE_OOP
#define PROFILE_OOP

#include <defs.oop>

#if (DOUBLE)
   #define PRECISION   double
   #define Profile     dProfile
   #define MACHINE_EPS dEPS
#else
   #define PRECISION   float
   #define Profile     fProfile
   #define MACHINE_EPS fEPS
#endif

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * RCS Log:
 *
 * $Log$
 *========================================================================*/
/* Profile class declaration                                              */
/*========================================================================*/
/*
   This pure virtual base class provides a uniform interface to a family of
   derived classes whose purpose is to model an arbitrary general
   mathematical function of the form: y=f(x).  This base class provides no
   functionality except as a repository for a few data members...its purpose
   is to unify the interface for defined classes.

   written by Dana Swift
*/
class Profile: public SwiftWare
{
   // member data
   private:

      // define profile fingerprint
      static char const *fingerprint;

   public:

      // define variables to contain extremes
      PRECISION xmin,xmax,ymin,ymax;

   // member functions
   protected:

      // function to copy data members
      inline virtual void copy(const Profile &prf);
      
      // function to make a header from a fingerprint
      char *make_delimiter(const char *delimiter,const char *fingerprint_);

      // function to search a FILE stream for a delimiter string
      int seek_delimiter(char *delimiter,FILE *source) {return strseek(delimiter,source);}

   public:

      // constructor to initialize extremes
      Profile(void) {xmin=ymin=xmax=ymax=ERROR;}

      // copy constructor
      Profile(const Profile &prf) {copy(prf);}

      // assignment operator
      Profile &operator=(const Profile &prf) {copy(prf); return *this;}
      
      // overloaded operator functions
      virtual PRECISION operator() (PRECISION xtrg) {return Eval(xtrg);}

      // function to dump core to ostream
      virtual int DumpCore(ostream &dest=cout,const char id[]="",const char prefix[]="");

      // virtual declaration for profile evaluator
      virtual PRECISION Eval(PRECISION xtrg) = 0;

      // virtual declaration for inverting profile
      virtual PRECISION Invert(PRECISION ytrg);

      // virtual declaration for shape-shifter
      virtual int Learn(const PRECISION *x,const PRECISION *y,unsigned n) = 0;
      
      // overloaded function from SwiftWare base class
      virtual int ReadCore(istream &source);

      // virtual procedure for seeking extremes in data
      virtual void SeekExtremes(void) = 0;

      // virtual procedure for setting extremes in data
      virtual void SetExtremes(PRECISION xmin,PRECISION xmax,PRECISION ymin,PRECISION ymax);
};

/*------------------------------------------------------------------------*/
/* function to copy data members                                          */
/*------------------------------------------------------------------------*/
inline void Profile::copy(const Profile &prf)
{
   xmin=prf.xmin; xmax=prf.xmax; ymin=prf.ymin; ymax=prf.ymax;
}

/*------------------------------------------------------------------------*/
/* function to set the class' data members for data extremes              */
/*------------------------------------------------------------------------*/
inline void Profile::SetExtremes(PRECISION xmin,PRECISION xmax,PRECISION ymin,PRECISION ymax) 
{
   Profile::xmin=xmin; 
   Profile::xmax=xmax; 
   Profile::ymin=ymin; 
   Profile::ymax=ymax;
}

#endif // PROFILE_OOP
#ifdef PROFILE_CPP

// initialize fingerprint for the Profile class
const char *Profile::fingerprint="Profile";

/*------------------------------------------------------------------------*/
/* function to print out data member values                               */
/*------------------------------------------------------------------------*/
int Profile::DumpCore(ostream &dest,const char *id,const char *prefix)
{
   // write header to core file
   HeadCore(dest,id,prefix,fingerprint,IOSFLAGS|ios::scientific);

   // find the number of significant decimal digits
   int precision = -(int)log10(MACHINE_EPS);

   // print values of xmin and xmax
   dest << indent << "PRECISION (xmin,xmax) = ("
        << setw(precision+8) << setprecision(precision) << xmin << ','
        << setw(precision+8) << setprecision(precision) << xmax << ")\n";

   // print values of ymin and ymax
   dest << indent << "PRECISION (ymin,ymax) = ("
        << setw(precision+8) << setprecision(precision) << ymin << ','
        << setw(precision+8) << setprecision(precision) << ymax << ")\n";

   // write trailer to core file
   TailCore();

   return 1;
}
 
/*------------------------------------------------------------------------*/
/* stub for virtual function to invert profile                            */
/*------------------------------------------------------------------------*/
PRECISION Profile::Invert(PRECISION)
{
   message("error in Profile::Invert()...Noninvertible profile.\n");

   return ERROR;
}

/*------------------------------------------------------------------------*/
/* function to construct a file delimiter from a fingerprint              */
/*------------------------------------------------------------------------*/
char *Profile::make_delimiter(const char *fingerprint_,const char *delimiter)
{
   // reserve static room for delimiter
   static char hdr[MAXSTRLEN];

   // create delimiter in static buffer
   sprintf(hdr,"$%s::%s",fingerprint_,delimiter);

   // return the pointer 
   return hdr;

   #undef MAXSTRLEN
}

/*------------------------------------------------------------------------*/
/* function to parse a core file                                          */
/*------------------------------------------------------------------------*/
int Profile::ReadCore(istream &source)
{
   char *loc; const char *key;
   int len;

   // make sure Profile class identifier was found
   key = "Profile::"; if (!(loc = strloc(source,key,NULL))) return 0;

   // the fingerprint comes next but skip that and extract xmin and xmax
   key = "PRECISION (xmin,xmax) = ("; len=strlen(key);
   if (!(loc = strloc(source,key,NULL))) swifterr("error in Profile::ReadCore()...key \"%s\" not found.\n",key);
   else {loc+=len; xmin=atof(strtok(loc,",")); xmax=atof(strtok(NULL,")"));}

   // extract ymin and ymax
   key = "PRECISION (ymin,ymax) = ("; len=strlen(key);
   if (!(loc = strloc(source,key,NULL))) swifterr("error in Profile::ReadCore()...key \"%s\" not found.\n",key);
   else {loc+=len; ymin=atof(strtok(loc,",")); ymax=atof(strtok(NULL,")"));}

   return 1;
}

#endif // PROFILE_CPP
