.DEFAULT:
.SUFFIXES:
.PHONY: force
.PRECIOUS: %.o
SHELL=/bin/csh

binFiles := ../bin/rz ../bin/sz ../bin/chkconfig ../bin/FwRev
symFiles := crc16bit.c logger.c serial.c inrange.c gmtime.c auxtime.c nan.c \
            extract.c cd.c
hdrFiles := crc16bit.h logger.h serial.h config.h linux.h linuxsp.h inrange.h \
            mcu.h gmtime.h auxtime.h ds2740.h fatio.h fatsys.h nan.h extract.h \
            cd.h stream.h ff.h
objFiles := $(subst .c,.o, $(symFiles))

all: symfiles headers swiftware $(binFiles) ebm manual
	@echo Done making all...

../bin/chkconfig: chkconfig.o config.o logger.o crc16bit.o linux.o gmtime.o ff.o
	gcc -m32 -ggdb -L. $^ -lm -lSwiftWare -static -o $@

../bin/rz: rz.o auxtime.o logger.o crc16bit.o serial.o StdioPort.o linux.o linuxsp.o \
           gmtime.o cd.o nan.o
	gcc -m32 $^ -L. -lzmodem -lg++ -lSwiftWare++ -lSwiftWare -lstdc++ -lm -static -o $@

../bin/sz: sz.o auxtime.o crc16bit.o logger.o serial.o StdioPort.o linux.o linuxsp.o \
           gmtime.o cd.o nan.o
	gcc -m32 $^ -L. -lzmodem -lg++ -lSwiftWare++ -lSwiftWare -lstdc++ -lm -static -o $@

../bin/FwRev: FwRev.o
	gcc -m32 $^ -o $@

clean: clean-swiftware clean-ebm clean-manual force
	-rm -f $(binFiles) $(addsuffix .o,$(binFiles)) $(symFiles) $(objFiles) $(hdrFiles) \
          Tx.o Rx.o chkconfig.o config.o linux.o rz.o sz.o StdioPort.o FwRev.o linuxsp.o \
          ff.o $(wildcard *.diff)

%.o: %.c
	gcc -m32 -D$(shell echo $< | tr a-z.- A-Z__) -Dpersistent="" -c -x c -I{.,SwiftWare/{c,zlib}} \
       -ggdb -static -Wall -Wmissing-prototypes -Wno-misleading-indentation $< -o $@

%.o: %.cpp
	gcc -m32 -D$(shell echo $< | tr a-z.- A-Z__) -c -x c++ -ggdb -I{.,SwiftWare/{c,c++,g++,zlib}} \
       -Wall -Wno-misleading-indentation $*.cpp -o $*.o 

config.o: config.c
	gcc -m32 -D$(shell echo $< | tr a-z.- A-Z__) -DWDogTimeOut=6300 -Dpersistent="" \
       -c -x c -I{.,SwiftWare/c} -ggdb -fno-builtin -Wall -Wmissing-prototypes $< -o $@

headers: $(hdrFiles)

%.h: %.c 
	ln -s $< $@

symfiles: $(symFiles)

$(symFiles):
	-ln -s ../lib/$@

config.c: ../src/config.c
	-ln -s ../src/$@

manual: force
	-cd ../manual; make pdf

clean-manual: force
	-cd ../manual; make clean

ebm: force
	-cd ../ebm; make

clean-ebm: force
	-cd ../ebm; make clean

swiftware: force
	-cd SwiftWare/c; make
	-cd SwiftWare/c++; make
	-cd SwiftWare/g++; make
	-cd SwiftWare/src; make
	-cd SwiftWare/zlib; make

clean-swiftware: force
	-cd SwiftWare/c; make clean
	-cd SwiftWare/c++; make clean
	-cd SwiftWare/g++; make clean
	-cd SwiftWare/src; make clean
	-cd SwiftWare/zlib; make clean

mcu.h fatio.h fatsys.h ds2740.h:
	ln -s ../apflib/$(basename $@).c $@

stream.h: 
	ln -s ../lib/$(basename $@).c $@

linux.o: linux.c
	gcc -m32 -D$(shell echo $<|tr a-z.- A-Z__) -Dpersistent="" \
       -c -x c -I. -ggdb  -I. -Wall -Wmissing-prototypes $< -o $@

chkconfig.o: chkconfig.c
	gcc -m32 -D$(shell echo $<|tr a-z.- A-Z__) -Dpersistent="" \
       -DFWREV=0x`perl -ne 's/.*revision:[ ]*([0-9]+)/\1/g;print' ../src/FirmwareRevision` \
       -c -x c -I{.,SwiftWare/c} -ggdb -Wall -Wmissing-prototypes $< -o $@

diff: $(subst .c,.diff, $(filter-out config.c, $(wildcard *.c))) \
      $(subst .cpp,.diff, $(wildcard *.cpp))
	@echo Done making $@ ...

difDir = ../../Apf11Sbe41cpBoilerPlate
%.diff: %.c force
	diff {,$(difDir)/support/}$< > $@ || true

%.diff: %.cpp force
	diff {,$(difDir)/support/}$< > $@ || true
