#ifndef LINUXSP_H
#define LINUXSP_H

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/**
 *
 * RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
#define linuxspChangeLog "$RCSfile$  $Revision$  $Date$"

#include <limits.h>
#include <serial.h>

/* declarations for functions with external linkage */
int makeraw(const struct SerialPort *com);
int sclose(const struct SerialPort *com);
const struct SerialPort *sopen(const char *dev, int baud, int rtscts);
int TxBreak(const struct SerialPort *com,int millisec);

/* declarations of SerialPort objects with external linkage */
extern const struct SerialPort init;

/* define sentinel values for serial port config() command */
#define ComFdesQuery     (LONG_MAX)
#define ComFdesInit      (LONG_MIN)
#define ComQueryBaudRate (LONG_MIN+1L)
#define ComDisable       (0L)

#endif /* LINUXSP_H */
#ifdef LINUXSP_C
#undef LINUXSP_C

#include <errno.h>
#include <fcntl.h>
#include <logger.h>
#include <signal.h>
#include <string.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

/* define an array to contain file descriptors */
static int fdes[6]={-1,-1,-1,-1,-1,-1};

/* define objects to implement a stack-based resource manager */
static int stackbuilt=0;
static const struct SerialPort *stack[6]={0,0,0,0,0,0};

static int cd0(void);
static int cd1(void);
static int cd2(void);
static int cd3(void);
static int cd4(void);
static int cd5(void);

static long int config0(long mode);
static long int config1(long mode);
static long int config2(long mode);
static long int config3(long mode);
static long int config4(long mode);
static long int config5(long mode);

static int cts0(void);
static int cts1(void);
static int cts2(void);
static int cts3(void);
static int cts4(void);
static int cts5(void);

static int dsr0(void);
static int dsr1(void);
static int dsr2(void);
static int dsr3(void);
static int dsr4(void);
static int dsr5(void);

static int dtr0(int state);
static int dtr1(int state);
static int dtr2(int state);
static int dtr3(int state);
static int dtr4(int state);
static int dtr5(int state);

static int getb0(unsigned char *byte);
static int getb1(unsigned char *byte);
static int getb2(unsigned char *byte);
static int getb3(unsigned char *byte);
static int getb4(unsigned char *byte);
static int getb5(unsigned char *byte);

static int ibytes0(void);
static int ibytes1(void);
static int ibytes2(void);
static int ibytes3(void);
static int ibytes4(void);
static int ibytes5(void);

static int iflush0(void);
static int iflush1(void);
static int iflush2(void);
static int iflush3(void);
static int iflush4(void);
static int iflush5(void);

static int ioflush0(void);
static int ioflush1(void);
static int ioflush2(void);
static int ioflush3(void);
static int ioflush4(void);
static int ioflush5(void);

static long na_config(long mode);
static int  na_getb(unsigned char *byte);
static int  na_putb(unsigned char byte);
static int  na_iflush(void);
static int  na_ioflush(void);
static int  na_oflush(void);
static int  na_ibytes(void);
static int  na_obytes(void);
static int  na_cd(void);
static int  na_rts(int state);
static int  na_cts(void);
static int  na_dtr(int state);
static int  na_dsr(void);

static int obytes0(void);
static int obytes1(void);
static int obytes2(void);
static int obytes3(void);
static int obytes4(void);
static int obytes5(void);

static int oflush0(void);
static int oflush1(void);
static int oflush2(void);
static int oflush3(void);
static int oflush4(void);
static int oflush5(void);

static int putb0(unsigned char byte);
static int putb1(unsigned char byte);
static int putb2(unsigned char byte);
static int putb3(unsigned char byte);
static int putb4(unsigned char byte);
static int putb5(unsigned char byte);

static int rts0(int state);
static int rts1(int state);
static int rts2(int state);
static int rts3(int state);
static int rts4(int state);
static int rts5(int state);

/* define an initialization object */
const struct SerialPort init={na_getb,na_putb,na_iflush,na_ioflush,
                              na_oflush,na_ibytes,na_obytes,na_cd,
                              na_rts,na_cts,na_dtr,na_dsr,na_config};

/* define SerialPort objects */
struct SerialPort com[6]={ 
   {getb0,putb0,iflush0,ioflush0,oflush0,ibytes0,obytes0,cd0,rts0,cts0,dtr0,dsr0,config0},
   {getb1,putb1,iflush1,ioflush1,oflush1,ibytes1,obytes1,cd1,rts1,cts1,dtr1,dsr1,config1},
   {getb2,putb2,iflush2,ioflush2,oflush2,ibytes2,obytes2,cd2,rts2,cts2,dtr2,dsr2,config2},
   {getb3,putb3,iflush3,ioflush3,oflush3,ibytes3,obytes3,cd3,rts3,cts3,dtr3,dsr3,config3},
   {getb4,putb4,iflush4,ioflush4,oflush4,ibytes4,obytes4,cd4,rts4,cts4,dtr4,dsr4,config4},
   {getb5,putb5,iflush5,ioflush5,oflush5,ibytes5,obytes5,cd5,rts5,cts5,dtr5,dsr5,config5},
};

static int      cd(int fdes);
static long int config(int fdes,long mode);
static int      cts(int fdes);
static int      dsr(int fdes);
static int      dtr(int state,int fdes);
static int      getb(unsigned char *byte,int fdes);
static int      ibytes(int fdes);
static int      iflush(int fdes);
static int      ioflush(int fdes);
static int      obytes(int fdes);
static int      oflush(int fdes);
static int      putb(unsigned char byte,int fdes);
static int      rts(int state,int fdes);

static int cd0(void) {return cd(fdes[0]);}
static int cd1(void) {return cd(fdes[1]);}
static int cd2(void) {return cd(fdes[2]);}
static int cd3(void) {return cd(fdes[3]);}
static int cd4(void) {return cd(fdes[4]);}
static int cd5(void) {return cd(fdes[5]);}
  
static long int config0(long mode) {return config(((mode<0 || mode==LONG_MAX)?0:fdes[0]),mode);}
static long int config1(long mode) {return config(((mode<0 || mode==LONG_MAX)?1:fdes[1]),mode);}
static long int config2(long mode) {return config(((mode<0 || mode==LONG_MAX)?2:fdes[2]),mode);}
static long int config3(long mode) {return config(((mode<0 || mode==LONG_MAX)?3:fdes[3]),mode);}
static long int config4(long mode) {return config(((mode<0 || mode==LONG_MAX)?4:fdes[4]),mode);}
static long int config5(long mode) {return config(((mode<0 || mode==LONG_MAX)?5:fdes[5]),mode);}

static int cts0(void) {return cts(fdes[0]);}
static int cts1(void) {return cts(fdes[1]);}
static int cts2(void) {return cts(fdes[2]);}
static int cts3(void) {return cts(fdes[3]);}
static int cts4(void) {return cts(fdes[4]);}
static int cts5(void) {return cts(fdes[5]);}

static int dsr0(void) {return dsr(fdes[0]);}
static int dsr1(void) {return dsr(fdes[1]);}
static int dsr2(void) {return dsr(fdes[2]);}
static int dsr3(void) {return dsr(fdes[3]);}
static int dsr4(void) {return dsr(fdes[4]);}
static int dsr5(void) {return dsr(fdes[5]);}

static int dtr0(int state) {return dtr(state,fdes[0]);}
static int dtr1(int state) {return dtr(state,fdes[1]);}
static int dtr2(int state) {return dtr(state,fdes[2]);}
static int dtr3(int state) {return dtr(state,fdes[3]);}
static int dtr4(int state) {return dtr(state,fdes[4]);}
static int dtr5(int state) {return dtr(state,fdes[5]);}

static int getb0(unsigned char *byte) {return getb(byte,fdes[0]);}
static int getb1(unsigned char *byte) {return getb(byte,fdes[1]);}
static int getb2(unsigned char *byte) {return getb(byte,fdes[2]);}
static int getb3(unsigned char *byte) {return getb(byte,fdes[3]);}
static int getb4(unsigned char *byte) {return getb(byte,fdes[4]);}
static int getb5(unsigned char *byte) {return getb(byte,fdes[5]);}

static int ibytes0(void) {return ibytes(fdes[0]);}
static int ibytes1(void) {return ibytes(fdes[1]);}
static int ibytes2(void) {return ibytes(fdes[2]);}
static int ibytes3(void) {return ibytes(fdes[3]);}
static int ibytes4(void) {return ibytes(fdes[4]);}
static int ibytes5(void) {return ibytes(fdes[5]);}

static int iflush0(void) {return iflush(fdes[0]);}
static int iflush1(void) {return iflush(fdes[1]);}
static int iflush2(void) {return iflush(fdes[2]);}
static int iflush3(void) {return iflush(fdes[3]);}
static int iflush4(void) {return iflush(fdes[4]);}
static int iflush5(void) {return iflush(fdes[5]);}

static int ioflush0(void) {return ioflush(fdes[0]);}
static int ioflush1(void) {return ioflush(fdes[1]);}
static int ioflush2(void) {return ioflush(fdes[2]);}
static int ioflush3(void) {return ioflush(fdes[3]);}
static int ioflush4(void) {return ioflush(fdes[4]);}
static int ioflush5(void) {return ioflush(fdes[5]);}

static long na_config(long mode)         {LogEntry("config()", "Not implemented.\n"); return -1;}
static int  na_getb(unsigned char *byte) {LogEntry("getb()",   "Not implemented.\n"); return -1;}
static int  na_putb(unsigned char byte)  {LogEntry("putb()",   "Not implemented.\n"); return -1;}
static int  na_iflush(void)              {LogEntry("iflush()", "Not implemented.\n"); return -1;}
static int  na_ioflush(void)             {LogEntry("ioflush()","Not implemented.\n"); return -1;}
static int  na_oflush(void)              {LogEntry("oflush()", "Not implemented.\n"); return -1;}
static int  na_ibytes(void)              {LogEntry("ibytes()", "Not implemented.\n"); return -1;}
static int  na_obytes(void)              {LogEntry("obytes()", "Not implemented.\n"); return -1;}
static int  na_cd(void)                  {LogEntry("cd()",     "Not implemented.\n"); return -1;}
static int  na_rts(int state)            {LogEntry("rts()",    "Not implemented.\n"); return -1;}
static int  na_cts(void)                 {LogEntry("cts()",    "Not implemented.\n"); return -1;}
static int  na_dtr(int state)            {LogEntry("dtr()",    "Not implemented.\n"); return -1;}
static int  na_dsr(void)                 {LogEntry("dsr()",    "Not implemented.\n"); return -1;}

static int obytes0(void) {return obytes(fdes[0]);}
static int obytes1(void) {return obytes(fdes[1]);}
static int obytes2(void) {return obytes(fdes[2]);}
static int obytes3(void) {return obytes(fdes[3]);}
static int obytes4(void) {return obytes(fdes[4]);}
static int obytes5(void) {return obytes(fdes[5]);}

static int oflush0(void) {return oflush(fdes[0]);}
static int oflush1(void) {return oflush(fdes[1]);}
static int oflush2(void) {return oflush(fdes[2]);}
static int oflush3(void) {return oflush(fdes[3]);}
static int oflush4(void) {return oflush(fdes[4]);}
static int oflush5(void) {return oflush(fdes[5]);}

static int putb0(unsigned char byte) {return putb(byte,fdes[0]);}
static int putb1(unsigned char byte) {return putb(byte,fdes[1]);}
static int putb2(unsigned char byte) {return putb(byte,fdes[2]);}
static int putb3(unsigned char byte) {return putb(byte,fdes[3]);}
static int putb4(unsigned char byte) {return putb(byte,fdes[4]);}
static int putb5(unsigned char byte) {return putb(byte,fdes[5]);}

static int rts0(int state) {return rts(state,fdes[0]);}
static int rts1(int state) {return rts(state,fdes[1]);}
static int rts2(int state) {return rts(state,fdes[2]);}
static int rts3(int state) {return rts(state,fdes[3]);}
static int rts4(int state) {return rts(state,fdes[4]);}
static int rts5(int state) {return rts(state,fdes[5]);}

static int makestack(void);
static int pop(void);
static int push(const struct SerialPort *com);
static const struct SerialPort *top(void);

/*------------------------------------------------------------------------*/
/* fucntion to convert serial stream to raw mode                          */
/*------------------------------------------------------------------------*/
/**
   This function induces an open SerialPort connection into raw mode.
   Raw mode disables processing of both transmitted/received streams,
   signal generation, and echoing.  If successful, this function
   returns a positive value or zero, on failure.  A negative return
   value indicates that an exception was detected.
*/
int makeraw(const struct SerialPort *com)
{
   /* define the logging signature */
   cc *FuncName="makeraw()";

   /* initialize the return status */
   int fd,status=-1;

   /* validate the SerialPort object */
   if (!com) {LogEntry(FuncName,"SerialPort is not open (com=NULL).\n");}

   /* query the serial port for its file descriptor */
   else if ((fd=com->config(LONG_MAX))<=0)
   {
      LogEntry(FuncName,"Query for SerialPort's file "
               "descriptor failed. [err=%d]\n",fd);
   }

   else
   {
      /* get termio parms */         
      struct termios ioport; tcgetattr(fd,&ioport);

      /* disable input attributes */
      ioport.c_iflag &= ~(INPCK | IGNPAR | PARMRK | ISTRIP | BRKINT | IGNCR |
                          ICRNL | INLCR  | IXOFF  | IXON   | IXANY  | IMAXBEL);

      /* enable input attributes */
      ioport.c_iflag |=  (IGNBRK);

      /* disable output attributes */
      ioport.c_oflag &= ~(OPOST | ONLCR);

      /* disable control attributes */
      ioport.c_cflag &= ~(HUPCL | CSTOPB | PARENB | PARODD | CSIZE);
      
      /* enable control attributes */
      ioport.c_cflag |=  (CLOCAL | CREAD | CS8);

      /* disable local attributes */
      ioport.c_lflag &= ~(ICANON  | ECHO | ECHOE  | ECHOPRT | ECHOK  | ECHOKE |
                          ECHOCTL | ISIG | IEXTEN | TOSTOP  | FLUSHO | PENDIN);

      /* enable local attributes */
      ioport.c_lflag |= (NOFLSH);

      /* reprogram the serial port */
      if (!tcsetattr(fd,TCSANOW,&ioport)) {status=1;}

      /* make a log entry */
      else {LogEntry(FuncName,"tcsetattr() failed for fdes(%d): %s\n",
                     fd,strerror(errno)); status=0;}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the state of the carrier detect signal              */
/*------------------------------------------------------------------------*/
/**
   This function returns the state of the CD signal for a serial port.

      \begin{verbatim}
      input:

         fdes....The file descriptor that is associated with the serial
                 port.
      output:

         This function returns a positive value if the CD signal is
         asserted and zero if the CD signal is cleared.  A negative return
         value indicates the file descriptor was invalid (ie., the serial
         port was not open) or else that an exception was encountered.
      \end{verbatim}
*/
static int cd(int fdes)
{
   /* initialize the SerialPort structure */
   cc *FuncName="cd()";

   /* initialize the return status */
   int status=-1;

   /* check if the serial port is open */
   if (fdes<0) {LogEntry(FuncName,"Serial port is not open.\n");}

   /* query the status of the CD signal */
   else if (ioctl(fdes,TIOCMGET,&status))
   {
      LogEntry(FuncName,"Unable to determine state of CD pin for fdes(%d).\n",fdes);
      status=-1;
   }

   /* determine the state of the CD signal */
   else {status=(status&TIOCM_CD) ? 1 : 0;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to control the SerialPort's configuration functionality       */
/*------------------------------------------------------------------------*/
/**
   This function implements the ability to set, get, and query the
   file descriptor that is associated with the SerialPort and to
   adjust the baud rate.

      \begin{verbatim}
      input:

         mode...This parameter controls functionality.

                 - The value LONG_MAX is a sentinel value that
                   requests the file descriptor to be returned.
                 - The value LONG_MIN is a sentinel value that requests
                   that the file descriptor be initialized to -1.
                 - The value LONG_MIN+1 is a sentinel value that
                   requests the baud rate of the serial port.
                 - Negative values less than or equal to CHAR_MIN are
                   sentinel values that returns a positive value
                   indicating that the serial port is enabled.
                 - A negative value requests that the file descriptor be
                   initialized with its absolute value.
                 - A zero emulates the command to disable the com port
                   without actually doing so.
                 - A positive value requests that the baud rate of the
                   serial port be changed to match.

         fd......The file descriptor associated with a SerialPort.

      output:

         This function returns a positive value on success and zero on
         failure.  A negative value indicates an exception.
      \end{verbatim}
*/
static long int config(int fd,long mode)
{
   /* define the logging signature */
   cc *FuncName="config()";

   /* initialize the return status */
   long int status=-1;

   /* validate the file descriptor */
   if (fd<0) {LogEntry(FuncName,"Serial port is not open.\n"); status=-1;}

   else
   {
      /* check for request to return the file descriptor */
      if (mode==ComFdesQuery) {status=fdes[fd];}
   
      /* check for request to initialize the file descriptor */
      else if (mode==ComFdesInit) {fdes[fd]=-1; status=1;}

      /* check for query to return baud rate */
      else if (mode==ComQueryBaudRate) 
      {
         /* get termio parms */         
         struct termios ioport; tcgetattr(fd,&ioport);

         /* select the baud rate */
         switch (cfgetispeed(&ioport))
         {
            case B300:    {status=300;    break;}
            case B600:    {status=600;    break;}
            case B1200:   {status=1200;   break;}
            case B2400:   {status=2400;   break;}
            case B4800:   {status=4800;   break;}
            case B9600:   {status=9600;   break;}
            case B19200:  {status=19200;  break;}
            case B38400:  {status=38400;  break;}
            case B57600:  {status=57600;  break;}
            case B115200: {status=115200; break;}
            default:      {status=9600;}
         }
      }
      
      /* check for request to determine if com port is enabled */
      else if (mode<=CHAR_MIN) {status=(dsr(fdes[fd])>0)?1:0;}
      
      /* check for request to associate a SerialPort with a file descriptor */
      else if (mode<0)  {fdes[fd]=-mode; status=1;}

      /* clear DTR to disable the com port */
      else if (!mode) {dtr(0,fd); status=1;}

      else
      {
         /* select the baud rate */
         switch (mode)
         {
            case 300:    {mode=B300;    break;}
            case 600:    {mode=B600;    break;}
            case 1200:   {mode=B1200;   break;}
            case 2400:   {mode=B2400;   break;}
            case 4800:   {mode=B4800;   break;}
            case 9600:   {mode=B9600;   break;}
            case 19200:  {mode=B19200;  break;}
            case 38400:  {mode=B38400;  break;}
            case 57600:  {mode=B57600;  break;}
            case 115200: {mode=B115200; break;}
            default:     {mode=0;}
         }
      
         /* set the speed */
         if (mode>0)
         {
            /* get termio parms */         
            struct termios ioport; tcgetattr(fd,&ioport);

            /* set the baud rate  */
            cfsetspeed(&ioport,mode);
               
            /* reprogram the serial port */
            if (tcsetattr(fd,TCSANOW,&ioport)<0)
            {
               /* make a log entry */
               LogEntry(FuncName,"tcsetattr() failed for fdes(%d): %s\n",
                        fd,strerror(errno));
               
               /* indicate failure */
               status=-1;
            }
            
            /* flush the IO buffers */
            else if (tcflush(fd,TCIOFLUSH)<0)
            {
               /* make a log entry */
               LogEntry(FuncName,"Attempt to flush IO buffers failed for fdes(%d): %s\n",
                        fd,strerror(errno));
               
               /* indicate failure */
               status=-1;
            }

            /* assert DTR */
            else {dtr(1,fd); status=1;}
         }

         /* indicate failure to adjust the baud rate */
         else {status=-1;}
      }
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to determine the state of the CTS signal                      */
/*------------------------------------------------------------------------*/
/**
   This function returns the state of the CTS signal for a serial port.

      \begin{verbatim}
      input:

         fdes....The file descriptor that is associated with the serial
                 port.
      output:

         This function returns a positive value if the CTS signal is
         asserted and zero if the CTS signal is cleared.  A negative return
         value indicates the file descriptor was invalid (ie., the serial
         port was not open) or else that an exception was encountered.
      \end{verbatim}
*/
static int cts(int fdes)
{
   /* define the logging signature */
   cc *FuncName="cts()";

   /* initialize the return status */
   int status=-1;

   /* check if the serial port is open */
   if (fdes<0) {LogEntry(FuncName,"Serial port is not open.\n");}

   /* query the status of the CTS signal */
   else if (ioctl(fdes,TIOCMGET,&status))
   {
      LogEntry(FuncName,"Unable to determine state of CTS pin for fdes(%d).\n",fdes);
      status=-1;
   }
   
   /* determine the state of the CTS signal */
   else {status=(status&TIOCM_CTS) ? 1 : 0;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to determine the state of the DSR signal                      */
/*------------------------------------------------------------------------*/
/**
   This function returns the state of the DSR signal for a serial port.

      \begin{verbatim}
      input:

         fdes....The file descriptor that is associated with the serial
                 port.
      output:

         This function returns a positive value if the DSR signal is
         asserted and zero if the DSR signal is cleared.  A negative return
         value indicates the file descriptor was invalid (ie., the serial
         port was not open) or else that an exception was encountered.
      \end{verbatim}
*/
static int dsr(int fdes)
{
   /* define the logging signature */
   cc *FuncName="dsr()";

   /* initialize the return status */
   int status=-1;

   /* check if the serial port is open */
   if (fdes<0) {LogEntry(FuncName,"Serial port is not open.\n");}

   /* query the status of the DSR signal */
   else if (ioctl(fdes,TIOCMGET,&status))
   {
      LogEntry(FuncName,"Unable to determine state of DSR pin for fdes(%d).\n",fdes);
      status=-1;
   }
   
   /* determine the state of the DSR signal */
   else {status=(status&TIOCM_DSR) ? 1 : 0;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to control the SerialPort's DTR signal and file descriptor    */
/*------------------------------------------------------------------------*/
/**
   This function controls the SerialPort's DTR signal associated with the
   SerialPort.

      \begin{verbatim}
      input:

         state...This parameter controls functionality.

                 - A zero requests that the DTR be cleared.
                 - A positive value requests that the DTR be asserted.

                 If state is zero then this function clears the DTR signal
                 If state is positive then this function asserts the DTR
                 signal.

         fd......The file descriptor associated with a SerialPort.

      output:

         This function returns a positive value on success and zero on
         failure.  A negative value indicates an exception.
      \end{verbatim}
*/
static int dtr(int state,int fdes)
{
   /* define the logging signature */
   cc *FuncName="dtr()";

   /* initialize the return status */
   int status=-1;

   /* define the DTR IO pin */
   const int iopin=TIOCM_DTR;

   /* check if the serial port is open */
   if (fdes<0) {LogEntry(FuncName,"Serial port is not open.\n");}

   /* check for request to clear the DTR signal */
   else if (!state)
   {
      /* clear the DTR signal */
      if (!ioctl(fdes,TIOCMBIC,&iopin)) {status=1;}

      /* log the failure */
      else {LogEntry(FuncName,"Failed attempt to clear DTR on fdes(%d).\n",fdes); status=-1;}
   }

   /* check for request to assert the DTR signal */
   else if (state>0)
   {
      /* assert the DTR signal */
      if (!ioctl(fdes,TIOCMBIS,&iopin)) {status=1;}

      /* log the failure */
      else {LogEntry(FuncName,"Failed attempt to assert DTR on fdes(%d).\n",fdes); status=-1;}
   }

   /* warn about an invalid DTR state */
   else {LogEntry(FuncName,"Invalid DTR state(%d) for file descriptor %d.\n",state,fdes);}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to read a byte from the serial port                           */
/*------------------------------------------------------------------------*/
/**
   This function reads a byte from the serial port.  

      \begin{verbatim}
      input:

         fdes.....The file descriptor associated with the SerialPort
                  object. 

      output:

         byte.....The byte read from the serial port.

         This function returns a positive value on success or zero on
         failure.  A negative value indicates an exception was detected.
      \end{verbatim}
*/
static int getb(unsigned char *byte,int fdes)
{
   /* define the logging signature */
   cc *FuncName="getb()";
  
   /* initialize the return status */
   int n,status=-1;

   /* validate the function argument */
   if (!byte) {LogEntry(FuncName,"NULL pointer to function parameter.\n");}

   /* check if the serial port is open */
   else if (fdes<0) {LogEntry(FuncName,"Serial port is not open.\n");}
   
   else
   {
      /* initialize the functions return values */
      status=0; *byte=0; 

      /* read a byte from the serial port */
      if (!ioctl(fdes,FIONREAD,&n) && n>0) {status=read(fdes,byte,1);}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to return the number of bytes in the input buffer             */
/*------------------------------------------------------------------------*/
/**
   This function is designed to read the number of bytes in the input
   buffer of the serial port.

      \begin{verbatim}
      input:
         fdes....The file descriptor associated with the serial port.

      output:

         This function returns a positive value on success or zero on
         failure.  A negative value indicates an exceptional condition.
      \end{verbatim}
*/
static int ibytes(int fdes)
{
   /* define the logging signature */
   cc *FuncName="ibytes()";

   /* initialize the return status */
   int n=-1;

   /* check if the serial port is open */
   if (fdes<0) {LogEntry(FuncName,"Serial port is not open.\n");}

   /* use ioctl() to determine number of bytes waiting in input queue */
   else if (ioctl(fdes,TIOCINQ,&n))
   {
      LogEntry(FuncName,"Unable to connect to fdes(%d).\n",fdes); n=-1;
   }
   
   return n;
}

/*------------------------------------------------------------------------*/
/* function to flush the bytes from the input buffer                      */
/*------------------------------------------------------------------------*/
/**
   This function is designed to flush bytes in the input buffer of the
   serial port.

      \begin{verbatim}
      input:
         fdes....The file descriptor associated with the serial port.

      output:

         This function returns a positive value on success or zero on
         failure.  A negative value indicates an exceptional condition.
      \end{verbatim}
*/
static int iflush(int fdes)
{
   /* define the logging signature */
   cc *FuncName="iflush()";

   /* initialize the return status */
   int status = -1;

   /* check if the serial port is open */
   if (fdes<0) {LogEntry(FuncName,"Serial port is not open.\n");}

   /* flush the input buffer */
   else if (tcflush(fdes,TCIFLUSH)<0)
   {
      LogEntry(FuncName,"%s\n",strerror(errno)); status=0;
   }

   /* indicate success */
   else status=1;

   return status;
}


/*------------------------------------------------------------------------*/
/* function to flush the bytes from the input and output buffers          */
/*------------------------------------------------------------------------*/
/**
   This function is designed to flush bytes from both the input and output
   buffers of the serial port.

      \begin{verbatim}
      input:
         fdes....The file descriptor associated with the serial port.

      output:

         This function returns a positive value on success or zero on
         failure.  A negative value indicates an exceptional condition.
      \end{verbatim}
*/
static int ioflush(int fdes)
{
   /* define the logging signature */
   cc *FuncName="ioflush()";

   /* initialize the return status */
   int status = -1;

   /* check if the serial port is open */
   if (fdes<0) {LogEntry(FuncName,"Serial port is not open.\n");}

   /* flush the input buffer */
   else if (tcflush(fdes,TCIOFLUSH)<0)
   {
      LogEntry(FuncName,"%s\n",strerror(errno)); status=0;
   }
   
   /* indicate success */
   else status=1;

   return status;
}

/*------------------------------------------------------------------------*/
/* function to construct the stack of SerialPort objects                  */
/*------------------------------------------------------------------------*/
/**
   This function constructs the stack of SerialPort objects.  This function
   returns a positive value on success or zero on failure.  A negative
   value indicates an exceptional condition.
*/
static int makestack(void)
{
   /* initialize the count of SerialPort objects */
   int n; const int N=sizeof(com)/sizeof(struct SerialPort);

   /* initialize the stack of SerialPorts */
   for (n=0; n<N; n++) stack[n]=(com+n);

   /* initialize the one-time switch */
   stackbuilt=1;
   
   return stackbuilt;
}

/*------------------------------------------------------------------------*/
/* function to return the number of bytes in the output buffer            */
/*------------------------------------------------------------------------*/
/**
   This function is designed to read the number of bytes in the output
   buffer of the serial port.

      \begin{verbatim}
      input:
         fdes....The file descriptor associated with the serial port.

      output:

         This function returns a positive value on success or zero on
         failure.  A negative value indicates an exceptional condition.
      \end{verbatim}
*/
static int obytes(int fdes)
{
   /* define the logging signature */
   cc *FuncName="obytes()";

   /* initialize the return status */
   int n=-1;

   /* check if the serial port is open */
   if (fdes<0) {LogEntry(FuncName,"Serial port is not open.\n");}

   /* use ioctl() to determine number of bytes waiting in output queue */
   else if (ioctl(fdes,TIOCOUTQ,&n))
   {
      LogEntry(FuncName,"Unable to connect to fdes(%d).\n",fdes); n=-1;
   }
   
   return n;
}


/*------------------------------------------------------------------------*/
/* function to flush the bytes from the output buffer                     */
/*------------------------------------------------------------------------*/
/**
   This function is designed to flush bytes in the output buffer of the
   serial port.

      \begin{verbatim}
      input:
         fdes....The file descriptor associated with the serial port.

      output:

         This function returns a positive value on success or zero on
         failure.  A negative value indicates an exceptional condition.
      \end{verbatim}
*/
static int oflush(int fdes)
{
   /* define the logging signature */
   cc *FuncName="oflush()";

   /* initialize the return status */
   int status = -1;

   /* check if the serial port is open */
   if (fdes<0) {LogEntry(FuncName,"Serial port is not open.\n");}

   /* flush the input buffer */
   else if (tcflush(fdes,TCOFLUSH)<0)
   {
      LogEntry(FuncName,"%s\n",strerror(errno)); status=0;
   }

   /* indicate success */
   else status=1;

   return status;
}

/*------------------------------------------------------------------------*/
/* function to pop the SerialPort object off the top of the stack         */
/*------------------------------------------------------------------------*/
/**
   This function pops the SerialPort object off the top of the stack.  This
   function returns a positive value on success or zero on failure.  A
   negative value indicates an exceptional condition.
*/
static int pop(void)
{
   /* initialize the count of SerialPort objects */
   int n,m; const int N=sizeof(com)/sizeof(struct SerialPort);

   /* initialize the return value */
   int status=0;

   /* loop through the stack to find the top */
   for (m=-1,n=0; n<N; n++) {if (stack[n]) {m=n;}}

   /* pop the SerialPort off the top of the stack */
   if (m>=0) {stack[m]=0; status=1;}
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to push a SerialPort on the top of the stack                  */
/*------------------------------------------------------------------------*/
/**
   This function pushes a SerialPort onto the top of the stack.

      \begin{verbatim}
      input:
         com......A structure that contains pointers to machine dependent
                  primitive IO functions.  See the comment section of the
                  SerialPort structure for details.  The function checks to
                  be sure this pointer is not NULL.

      output:

         This function returns a positive value on success or zero on
         failure.  A negative value indicates an exception was detected.
      \end{verbatim}
*/
static int push(const struct SerialPort *port)
{
   /* initialize the count of SerialPort objects */
   int n,m; const int N=sizeof(com)/sizeof(struct SerialPort);

   /* initialize the return value */
   int status=-1;

   /* match the function argument to the array of available SerialPorts */
   for (m=-1,n=0; n<N; n++) {if (port==(com+n)) {m=n;}}

   /* validate the SerialPort */
   if (m>=0 && m<N)
   {
      /* loop to validate the stack and find its top */
      for (status=0,m=-1,n=0; n<N; n++)
      {
         /* confirm that the SerialPort isn't already in the stack */
         if (port==stack[n]) break;

         /* locate the top of the stack */
         else if (!stack[n] && m<0) {m=n;}
      }

      /* push the SerialPort on the top of the stack */
      if (m>=0 && m<N) {stack[m]=port; status=1;}
   }
   
   return status;
}

/*------------------------------------------------------------------------*/
/* function to write a byte to the serial port                            */
/*------------------------------------------------------------------------*/
/**
   This function writes a byte to the serial port.  

      \begin{verbatim}
      input:

         fdes.....The file descriptor associated with the SerialPort
                  object. 

      output:

         byte.....The byte to be written to the serial port.

         This function returns a positive value on success or zero on
         failure.  A negative value indicates an exception was detected.
      \end{verbatim}
*/
static int putb(unsigned char byte,int fdes)
{
   /* define the logging signature */
   cc *FuncName="putb()";

   /* initialize the return status */
   int status=-1;

   /* check if the serial port is open */
   if (fdes<0) {LogEntry(FuncName,"Serial port is not open.\n");}

   /* write a byte to the serial port */
   else {byte&=0xff; status=write(fdes,&byte,1);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to control the SerialPort's RTS signal and file descriptor    */
/*------------------------------------------------------------------------*/
/**
   This function controls the SerialPort's RTS signal associated with the
   SerialPort.

      \begin{verbatim}
      input:

         state...This parameter controls functionality.

                 - A zero requests that the RTS be cleared.
                 - A positive value requests that the RTS be asserted.

                 If state is zero then this function clears the RTS signal
                 If state is positive then this function asserts the RTS
                 signal.

         fd......The file descriptor associated with a SerialPort.

      output:

         This function returns a positive value on success and zero on
         failure.  A negative value indicates an exception.
      \end{verbatim}
*/
static int rts(int state,int fdes)
{
   /* define the logging signature */
   cc *FuncName="rts()";

   /* initialize the return status */
   int status=-1;

   /* define the RTS IO pin */
   const int iopin=TIOCM_RTS;

   /* check if the serial port is open */
   if (fdes<0) {LogEntry(FuncName,"Serial port is not open.\n");}

   /* check for request to clear the RTS signal */
   else if (!state)
   {
      /* clear the RTS signal */
      if (!ioctl(fdes,TIOCMBIC,&iopin)) {status=1;}

      /* log the failure */
      else {LogEntry(FuncName,"Failed attempt to clear RTS on fdes(%d).\n",fdes); status=0;}
   }

   /* check for request to assert the RTS signal */
   else if (state>0)
   {
      /* assert the DTR signal */
      if (!ioctl(fdes,TIOCMBIS,&iopin)) {status=1;}

      /* log the failure */
      else {LogEntry(FuncName,"Failed attempt to assert RTS on fdes(%d).\n",fdes); status=0;}
   }
   
   /* warn about an invalid DTR state */
   else {LogEntry(FuncName,"Invalid RTS state(%d) for file descriptor %d.\n",state,fdes);}

   return status;
}

/*------------------------------------------------------------------------*/
/* function to close a SerialPort object                                  */
/*------------------------------------------------------------------------*/
/**
   This function closes a SerialPort.

      \begin{verbatim}
      input:
         com......A structure that contains pointers to machine dependent
                  primitive IO functions.  See the comment section of the
                  SerialPort structure for details.  The function checks to
                  be sure this pointer is not NULL.

      output:
         This function returns zero on success and a negative value on
         failure.
      \end{verbatim}
*/
int sclose(const struct SerialPort *com)
{
   /* initialize the SerialPort structure  */
   cc *FuncName="sclose()";

   /* initialize return value */
   int status=-1;

   /* validate the function parameter */
   if (!com) {LogEntry(FuncName,"NULL function parameter.\n");}

   /* validate the SerialPort's file descriptor */
   else if ((com->config(INT_MAX))<0) {LogEntry(FuncName,"Invalid SerialPort object.\n");}

   /* close the serial port */
   else if (!close(com->config(INT_MAX)))
   {
      /* reinitialize the SerialPort's file descriptor */
      com->config(INT_MIN); 

      /* push the SerialPort back on the stack */
      if (push(com)<=0) {LogEntry(FuncName,"Attempt failed to push the "
                                  "SerialPort onto the resource stack.\n");}

      /* indicate success */
      status=0;
   }

   /* log the error */
   else {LogEntry(FuncName,"Attempt to close serial port failed.\n");}
      
   return status;
}

/*------------------------------------------------------------------------*/
/* function to open a serial port as the data stream                      */
/*------------------------------------------------------------------------*/
/**
   This function opens the serial port and readies the SerialPort object
   for use.

      \begin{verbatim}
      input:

         dev......The path to the serial port device.

         baud.....The baud rate to be used for communications via the serial
                  port. 

         rtscts...Set true if RTS/CTS handshaking should be used.

      output:

         This function returns a pointer to a SerialPort object on success
         or NULL on failure.
      \end{verbatim}
*/
const struct SerialPort *sopen(const char *dev, int baud, int rtscts)
{
   /* initialize the SerialPort structure */
   cc *FuncName="sopen()";

   /* initialize the return object */
   const struct SerialPort *com=NULL;

   /* build the SerialPort resource stack */
   if (!stackbuilt) makestack();

   /* validate the function parameters */
   if (!dev) {LogEntry(FuncName,"NULL function parameter.\n");}

   else
   {
      /* define the structure to contain the serial port properties */
      struct termios ioport; int fdes=-1;

      /* open the serial port */
      if ((fdes=open(dev, O_RDWR | O_NOCTTY | O_NONBLOCK ))<0)
      {
         /* log the failure */
         LogEntry(FuncName,"Open failed: %s\n",dev);
      }

      /* set exclusive-use mode for the serial port */
      else if (ioctl(fdes,TIOCEXCL))
      {
         LogEntry(FuncName,"Unable to open in exclusive-use mode.\n"); fdes=-1;
      }

      /* set asychronous communications mode for the serial port */
      else if (fcntl(fdes,F_SETFL, O_RDWR | O_NOCTTY)<0)
      {
         LogEntry(FuncName,"Unable to open in asynchronous mode.\n"); fdes=-1;
      }
      
      else
      {
         /* get termio parms */
         tcgetattr(fdes,&ioport);
    
         /* set the flags that control the serial port behavior */
         ioport.c_oflag &= ~( OPOST | ONLCR );
         ioport.c_cflag &= ~( HUPCL | CSTOPB | PARENB | CSIZE | CRTSCTS | CS8 | CS7 );
         ioport.c_cflag |=  ( CLOCAL | CREAD | CS8 );
         ioport.c_lflag &= ~( ICANON | ECHO | ECHONL | ISIG | IEXTEN | FLUSHO );
         ioport.c_lflag |=  ( NOFLSH );
         ioport.c_iflag &= ~( INPCK | IGNPAR | PARMRK | ISTRIP | IGNBRK | BRKINT |
                              IGNCR | ICRNL  | INLCR  | IXOFF  | IXON   | IXANY  |
                              IMAXBEL );

         /* enable RTS/CTS hardware handshaking */
         if (rtscts) ioport.c_cflag |= (CRTSCTS);
  
         /* initialize input mode read() transfer parameters */
         ioport.c_cc[VMIN]=1; ioport.c_cc[VTIME]=0;

         /* select the baud rate */
         switch (baud)
         {
            case 300:    {baud=B300;    break;}
            case 600:    {baud=B600;    break;}
            case 1200:   {baud=B1200;   break;}
            case 2400:   {baud=B2400;   break;}
            case 4800:   {baud=B4800;   break;}
            case 9600:   {baud=B9600;   break;}
            case 19200:  {baud=B19200;  break;}
            case 38400:  {baud=B38400;  break;}
            case 57600:  {baud=B57600;  break;}
            case 115200: {baud=B115200; break;}
            default:
            {
               LogEntry(FuncName,"Unsupported baud rate(%d baud); setting to 19200baud.\n",baud);
               baud=B19200;
            }
         }
      
         /* set the speed */
         cfsetspeed(&ioport,baud);
   
         /* reprogram the serial port */
         if (tcsetattr(fdes,TCSANOW,&ioport)<0)
         {
            /* make a log entry */
            LogEntry(FuncName,"tcsetattr() failed: %s\n",strerror(errno));

            /* close the serial port */
            close(fdes); fdes=-1;
         }

         /* flush the IO buffers */
         else if (tcflush(fdes,TCIOFLUSH)<0)
         {
            /* make a log entry */
            LogEntry(FuncName,"Attempt to flush IO buffers failed: %s\n",strerror(errno));

            /* close the serial port */
            close(fdes); fdes=-1;
         }
      }

      /* verify that the serial port */
      if (fdes>=0)
      {
         /* check if a SerialPort is available */
         if ((com=top())) {com->config(-fdes); pop();}
         else
         {
            /* make a log entry */
            LogEntry(FuncName,"SerialPort resource unavailable.\n");
            
            /* close the serial port */
            close(fdes); fdes=-1;
         }
      }
   }

   return com;
}

/*------------------------------------------------------------------------*/
/* function to return the SerialPort on the top of the stack              */
/*------------------------------------------------------------------------*/
const static struct SerialPort *top(void)
{
   /* initialize the count of SerialPort objects */
   int n; const int N=sizeof(com)/sizeof(struct SerialPort);

   /* initialize the return value */
   const struct SerialPort *com=NULL;

   /* find the SerialPort on the top of the stack */
   for (n=0; n<N; n++) {if (stack[n]) {com=stack[n];}}

   return com;
}

/*------------------------------------------------------------------------*/
/* function to send a break signal for a specified duration of time       */
/*------------------------------------------------------------------------*/
/**
   This function sends a break condition of specified duration via the
   Tx signal.

      \begin{verbatim}
      input:
         com........A structure that contains pointers to machine
                    dependent primitive IO functions.  See the comment
                    section of the SerialPort structure for details.
                    The function checks to be sure this pointer is not
                    NULL.

         millisec...This specifies the minimum duration (in
                    milliseconds) of the break signal.  The actual
                    duration may be somewhat longer.

      output:
         This function returns a positive value on success or zero if
         the serial port is not open.  A negative return value
         indicates that an exception was encountered.
      \end{verbatim}
*/
int TxBreak(const struct SerialPort *com,int millisec)
{
   /* initialize the SerialPort structure */
   cc *FuncName="TxBreak()";

   /* initialize the return value */
   int fd,status=-1;

   /* validate the duration of the break signal */
   if (millisec>0)
   {
      /* validate the function parameter */
      if (!com || !com->config) {LogEntry(FuncName,"Invalid function argument.\n");}

      /* request the file descriptor for the serial port */
      else if ((fd=com->config(INT_MAX))<0)
      {
         LogEntry(FuncName,"Serial port not activated.\n"); status=0;
      }

      /* send a break signal for the specified duration */
      else {tcsendbreak(fd,millisec); status=1;}
   }
   
   return status;
}

#endif /* LINUXSP_C */
