#include <defs.p>
#include <config.h>
#include <logger.h>
#include <string.h>

/* enumerate the FatFs volumes */
typedef enum {FatRoot,FatArchive} FatFsVolume;

int    MissionParametersWrite(struct MissionParameters *mission);
void   PowerOff(time_t AlarmSec);
int    RecoveryInit(void);
int    fioClean(void);
int    fioCreate(void);
int    fcloseall(void);
int    fformat(void);
int    fmove(const char *glob, FatFsVolume source);
time_t ialarm(void);

int    MissionParametersWrite(struct MissionParameters *mission) {return 1;}
void   PowerOff(time_t AlarmSec) {LogEntry("PowerOff()","Alarm set for %ld sec.\n",AlarmSec);}
int    RecoveryInit(void) {LogEntry("RecoveryInit()","Activating recovery mode.\n"); return 1;}
int    fioClean(void) {return 1;}
int    fioCreate(void) {return 1;}
int    fcloseall(void) {return 1;}
int    fformat(void) {return 1;}
int    fmove(const char *glob, FatFsVolume source) {return 1;}
time_t ialarm(void) {return 0;}

const unsigned long FwRev = FWREV;

/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * RCS Log:
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *========================================================================*/
/*                                                                        */
/*========================================================================*/
int main(int argc, char **argv)
{
   enum CMD {if_,cfg_,NCMD};
   struct MetaCmd cmd[NCMD]=
   {
      {"if=","  Pathname for configuration modifications.",0,0},
      {"cfg="," Pathname for the float's present configuration.",0,0}
   };
   struct MissionParameters config = DefaultMission;

   debugbits=2;
   
   /* check for usage query */
   if (argc<2) 
   { 
      printf("usage:%s %s\n",argv[0],make_usage(cmd,NCMD));
      exit(0);
   }

   /* link the metacommands to the command line arguments */
   link_meta_cmds(cmd,NCMD,argc,argv);

   if (!cmd[cfg_].arg)
   {
      LogEntry(argv[0],"Unspecified current configuration; "
               "assuming default configuration.\n");
   }
   else
   { 
      LogEntry(argv[0],"Validating the float's current configuration.\n");
      
      if (configure(&config,cmd[cfg_].arg)<=0)
      {
         /* log an entry that the configuration file is invalid */
         LogEntry(argv[0],"The float's present configuration is invalid.\n"); exit(1);
      }
      else LogEntry(argv[0],"The float's current configuration is accepted.\n");
   }
   
   /* make sure that an input file was specified */
   if (!cmd[if_].arg)
   {
      LogEntry(argv[0],"No input configuration file specified.\n");
   }

   /* check configuration file */
   else
   {
      LogEntry(argv[0],"\n");
      LogEntry(argv[0],"Validating the float's new configuration.\n");
      
      if (configure(&config,cmd[if_].arg)<=0)
      {
         /* log an entry that the configuration file is invalid */
         LogEntry(argv[0],"Configuration file invalid.\n");
      }
      
      /* log an entry that the configuration file is good */
      else {LogEntry(argv[0],"Configuration file OK.\n");}
   }
   
   return 0;
}

