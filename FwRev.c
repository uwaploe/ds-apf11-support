/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * $Id$
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Copyright University of Washington.   Written by Dana Swift.
 *
 * This software was developed at the University of Washington using funds
 * generously provided by the US Office of Naval Research, the National
 * Science Foundation, and NOAA.
 *  
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or (at
 * your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/**
 *
 * RCS log of revisions to the C source code.
 *
 * \begin{verbatim}
 * $Log$
 * \end{verbatim}
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

void PrintUsage(void);

/*========================================================================*/
/*                                                                        */
/*========================================================================*/
int main(int argc,char *argv[])
{
   char fwrev[32];
   time_t t=time(NULL);
   struct tm *tm=localtime(&t);
   int c;
   
   while ((c = getopt(argc, argv, "eho?")) != -1)
   {  
      switch (c)
      {
         /* check if the FwRev should specify an even day of the month */
         case 'e':
         {
            if ((tm->tm_mday&0x0001))
            {
               if (tm->tm_mon==1 && tm->tm_mday>=29) tm->tm_mday=28;
               else if (tm->tm_mday>=31) tm->tm_mday=30;
               else tm->tm_mday+=1;
            }
            break;
         }

         case '?':
         case 'h': {PrintUsage(); break;}
         
         /* check if the FwRev should specify an odd day of the month */
         case 'o':
         {
            if (!(tm->tm_mday&0x0001))
            {
               if (tm->tm_mon==1 && tm->tm_mday>=28) tm->tm_mday=27;
               else if (tm->tm_mday>=30) tm->tm_mday=29;
               else tm->tm_mday+=1;
            }
            break;
         }
      }
   }
   
   t=mktime(tm); tm=localtime(&t);

   strftime(fwrev,sizeof(fwrev),"%m%d%y",tm);

   printf("%s\n",fwrev);

   return 0;
}

/*------------------------------------------------------------------------*/
/*                                                                        */
/*------------------------------------------------------------------------*/
void PrintUsage(void)
{
   printf("usage:FwRev -e -h -o -?\n"
          "   -e     FwRev should specify an even day of the month.\n"
          "   -o     FwRev should specify an odd day of the month.\n"
          "   -h,-?  Print this usage summary.\n"
          "Note: The FwRev will specify today's date if -o or -e\n"
                 "options are not specified.\n\n");

   exit(0);
}

